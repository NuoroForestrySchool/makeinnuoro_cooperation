---
title: "Documenig problems in the use of measuRing package"
author:
- affiliation: NuoroForestrySchool
  name: "Roberto Scotti"
date: "05 febbraio  2019"
output:
  html_document: default
  html_notebook: default
  pdf_document: default
keywords: data wrangling
# subtitle: 
abstract: |
  Problems seem to be due to image digital structure
---

```r
library(measuRing)
```

```
## Package 'measuRing' version 0.5
```

```
## Type 'citation("measuRing")' for citing this R package in publications.
```

# Demo for basic use of the package


```r
#graphics.off()
#dev.new()
## (not run) Read two image sections in package measuRing:
image1 <- system.file("P105_a.tif", package="measuRing")
image2 <- system.file("P105_a.png", package="measuRing")
## (not run) compute a gray matrix:
gray <- imageTogray(image1)
## (not run) - the ppi is embedded in the image:
attributes(gray)
```

```
## $dim
## [1]   20 1610
## 
## $image
## [1] "C:/Users/ro/Documents/R/win-library/3.5/measuRing/P105_a.tif"
## 
## $ppi
## [1] 1000
## 
## $rgb
## [1] 0.3 0.6 0.1
## 
## $p.row
## [1] 1
```

```r
## (not run) but, the ppi is not embedded in image2:
## - imageTogray will return an error:
## (uncoment and run):
## gray2 <- imageTogray(image2)
## attributes(gray2)
## - the ppi should be provided (i.e. ppi = 1200):
gray3 <- imageTogray(image2,ppi = 1200)
attributes(gray3)
```

```
## $dim
## [1]   20 1610
## 
## $image
## [1] "C:/Users/ro/Documents/R/win-library/3.5/measuRing/P105_a.png"
## 
## $ppi
## [1] 1200
## 
## $rgb
## [1] 0.3 0.6 0.1
## 
## $p.row
## [1] 1
```

```r
##(not run) a plot of the gray matrix        
xrange <- range(0:ncol(gray)) + c(-1,1)
yrange <- range(0:nrow(gray)) + c(-1,1) 
plot(xrange,yrange,xlim=xrange,ylim=yrange,xlab='',
      ylab='',type='n',asp=0)
rasterImage(gray,xrange[1],yrange[1],xrange[2],yrange[2])
```

![plot of chunk imageTogray](figure/imageTogray-1.png)



```r
image1 <- system.file("P105_a.tif", package="measuRing")
## (not run) Initial diagnostic:
detect1 <- ringDetect(image1,segs=2, ratio = c(3.5, 7))
## (not run) Updating ringDetect to chage arguments;
## and flagged rings
detect1 <- update(detect1,marker=8) 
## (not run) Some noise in smoothed gray can be avoided
## by moving the origin: 
detect1 <- update(detect1,origin = -0.03)
## (not run) columns 21 and 130 are not considered now.
##
## (not run) Choose other columns in gray matrix (see ringSelect);
## (not run) graphical devices from ringDetect should be active!
## (not run) Including columns:
## (uncomment and run):
## detect1 <- update(detect1)
## Toinc <- ringSelect(detect1)
## detect1 <- update(detect1, inclu = Toinc)
## or, include the next columns: 
Toinc <- c(202,387,1564) 
detect1 <- update(detect1,inclu = Toinc)        
## (not run) Object detec1 is updated with Toinc;
##
## (not run)  ring borders to be excluded:
## (uncomment and run):
## detect1 <- update(detect1)
## Toexc <- ringSelect(detect1,any.col = FALSE)
## detect1 <- update(detect1,exclu=Toexc)
## or, exclude the nex columns: 
Toexc <- c(208,1444,1484)
detect1 <- update(detect1,exclu = Toexc)        
##
## (not run) Final arguments:
detect2 <- update(detect1,last.yr=2011,marker = 8)
str(detect2)
```

```
## List of 3
##  $ ringWidths :'data.frame':	65 obs. of  2 variables:
##   ..$ year  : num [1:65] 2011 2010 2009 2008 2007 ...
##   ..$ P105_a: num [1:65] 1.016 0.813 0.686 0.94 0.813 ...
##  $ ringBorders:'data.frame':	1610 obs. of  2 variables:
##   ..$ P105_a : num [1:1610] 0.0832 0.0962 0.1098 0.1211 0.1304 ...
##   ..$ borders: logi [1:1610] NA NA NA NA NA NA ...
##  $ call       : language ringDetect(image1, segs = 2, ratio = c(3.5, 7), marker = 8, origin = -0.03,      inclu = Toinc, exclu = Toexc, last.yr = 2011)
##  - attr(*, "gray.dim")= int [1:2] 20 1610
##  - attr(*, "opar")=List of 4
##   ..$ mfrow: num [1:2] 2 1
##   ..$ mar  : num [1:4] 3 2 3 2
##   ..$ oma  : num [1:4] 2 3 0 0
##   ..$ xpd  : logi NA
##  - attr(*, "coln")= chr [1:66] "10" "50" "82" "109" ...
```

```r
##
## (not run) kill previous plot:
graphics.off()
##
## (not run) Tree-ring widths and attributes:
rings <- detect2$'ringWidths'
##
## (not run) Plot of the tree-ring witdths:        
maint <- 'Hello ring widths!'
plot(rings,ylab = 'width (mm)',type='l',col = 'red',main=maint)
```



